#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */

/////////////////////////////////////////////////////////
// -------------------- maximum ---------------------- //
/////////////////////////////////////////////////////////
void testMaximum_7_5() {
    // Given
    int mayor;
    // When
    mayor = maximum(7,5);
    // Then
	assertEquals_int(7, mayor);
}

void testMaximum_3_9() {
    // Given
    int mayor;
    
    // When
    mayor = maximum(3,9);
    // Then
	assertEquals_int(9, mayor);
}

void testMaximum_4_4() {
    // Given
    int mayor;
    // When
    mayor = maximum(4,4);
    // Then
	assertEquals_int (4,mayor);
}
	
	
/////////////////////////////////////////////////////////
// -------------------- sumUpTo ---------------------- //
/////////////////////////////////////////////////////////
	
void testSumUpTo_minus1() {
    // Given
    int suma;
    // When
    suma = sumUpTo(-1);
    // Then
	assertEquals_int(0, suma);
}

void testSumUpTo_0() {
    // Given
    int suma;
    // When
    suma = sumUpTo(0);
    //Then
    assertEquals_int(0,suma);
	
}

void testSumUpTo_1() {
    // Given
    int suma;
    // When
    suma = sumUpTo(1);
    // Then
	assertEquals_int(1,suma);
}

void testSumUpTo_3() {
    // Given
    int suma;
    // When
    suma = sumUpTo(3);
    // Then
	assertEquals_int(0+1+2+3, suma);
}

void testSumUpTo_7() {
    // Given
    int suma;
    // When
    suma = sumUpTo(7);
    // Then
	assertEquals_int(0+1+2+3+4+5+6+7,suma);
}

	
/////////////////////////////////////////////////////////
// ---------------------- sum ------------------------ //
/////////////////////////////////////////////////////////
	
void testSum_Empty() {
    // Given
    int vector[0];
    int suma;
    // When
    suma = sum(vector, 0);
    // Then
	assertEquals_int(0, suma);
}

void testSum_5() {
    // Given
	int vector[] = {5};
	int suma;
	 // When
	 suma = sum(vector, 1);
	 // Then
     assertEquals_int(5, suma);
}

void testSum_5_7() {
    // Given
	int vector[] = {5,7};
    int suma;
    // When
	suma = sum(vector, 2);
	// Then
	assertEquals_int(12, suma);
}

void testSum_3_minus4_8() {
    // Given
	int vector[] = {3,-4,8};
	int suma;
	// When
	suma = sum(vector, 3);
	// Then
	assertEquals_int(7, suma);
}

void testSum_minus2_5_3_minus1_9() {
	int vector[] = {-2,5,3,-1,9};
	int suma;
	// When
    suma = sum(vector, 5);
	// Then
    assertEquals_int(14, suma);
}

void testSum_9_minus8_15_6_minus10_7_4() {
    // Given
	int vector[] = {9,-8,15,6,-10,7,4};
	int suma;
	// When
    suma = sum(vector, 7);
	// Then
	assertEquals_int(23, suma);
}
