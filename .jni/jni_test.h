/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _jni_test_h
#define _jni_test_h

#include "jni_inout.h"

#define fail(message) if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);fail_(message)

#define assertTrue(actual)    if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertTrue_(actual)
#define assertFalse(actual)   if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertFalse_(actual)

#define assertEquals_int(expected, actual)           if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertEquals_int_(expected, actual)
#define assertEquals_byte(expected, actual)          if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertEquals_byte_(expected, actual)
#define assertEquals_short(expected, actual)         if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertEquals_short_(expected, actual)
#define assertEquals_long(expected, actual)          if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertEquals_long_(expected, actual)
#define assertEquals_float(expected, actual, delta)  if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertEquals_float_(expected, actual, delta)
#define assertEquals_double(expected, actual, delta) if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertEquals_double_(expected, actual, delta)
#define assertEquals_bool(expected, actual)          if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertEquals_bool_(expected, actual)
#define assertEquals_char(expected, actual)          if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertEquals_char_(expected, actual)
#define assertEquals_String(expected, actual)        if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertEquals_String_(expected, actual)

#define assertArrayEquals_int(expected, expectedLength, actual, actualLength)           if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_int_(expected, expectedLength, actual, actualLength)
#define assertArrayEquals_byte(expected, expectedLength, actual, actualLength)          if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_byte_(expected, expectedLength, actual, actualLength)
#define assertArrayEquals_short(expected, expectedLength, actual, actualLength)         if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_short_(expected, expectedLength, actual, actualLength)
#define assertArrayEquals_long(expected, expectedLength, actual, actualLength)          if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_long_(expected, expectedLength, actual, actualLength)
#define assertArrayEquals_float(expected, expectedLength, actual, actualLength, delta)  if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_float_(expected, expectedLength, actual, actualLength, delta)
#define assertArrayEquals_double(expected, expectedLength, actual, actualLength, delta) if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_double_(expected, expectedLength, actual, actualLength, delta)
#define assertArrayEquals_bool(expected, expectedLength, actual, actualLength)          if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_bool_(expected, expectedLength, actual, actualLength)
#define assertArrayEquals_char(expected, expectedLength, actual, actualLength)          if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_char_(expected, expectedLength, actual, actualLength)
#define assertArrayEquals_String(expected, expectedLength, actual, actualLength)        if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_String_(expected, expectedLength, actual, actualLength)

#define assertMatrixEquals_int(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)    if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_int_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixEquals_byte(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)   if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_byte_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixEquals_short(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)  if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_short_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixEquals_long(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)   if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_long_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixEquals_float(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)  if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_float_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixEquals_double(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns) if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_double_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixEquals_bool(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)   if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_bool_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixEquals_char(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)   if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_char_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixEquals_String(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns) if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_String_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)

#define assertMatrixRegionEquals_int(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)    if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_int_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_byte(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)   if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_byte_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_short(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)  if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_short_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_long(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)   if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_long_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_float(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)  if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_float_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_double(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns) if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_double_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_bool(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)   if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_bool_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_char(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)   if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_char_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_String(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns) if (! javaEnv) return ; if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfo_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_String_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)

#include "jni_test.c"
#endif
