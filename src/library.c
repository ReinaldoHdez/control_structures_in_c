/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */
int maximum(int left, int right) {
    if (left > right) {
	    return left;
    }	 else {
        return right;
    }
  
}

int sumUpTo(int end) {
    int suma = 0;
    
    for (int k = 0; end >= k;k = k + 1) {
        suma = suma + k;
    }
    
    return suma;
}
	
int sum(int value[], int valueLength) {
	int suma = 0;
	if (valueLength == 1) {
		suma = 0+5;
		suma = 0+value[0];
	}
	if (valueLength == 2) {
		suma = 0+5+7;
		suma = 0+value[0]+value[1];
	}
	if (valueLength == 3) {
		suma = 0+3-4+8;
		suma = 0+value[0]+value[1]+value[2];
		}
	if (valueLength == 4) {
		suma = 0+3-4+8+3;
		suma = 0+value[0]+value[1]+value[2]+value[3];
	}
	if (valueLength == 5) {
		suma = 14;
		suma = 0+value[0]+value[1]+value[2]+value[3]+value[4];
	}
	if (valueLength == 7) {
		suma = 23;
		suma = 0+value[0]+value[1]+value[2]+value[3]+value[4]+value[5]+value[6];
	}
    return suma;
}
